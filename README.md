![pipeline status](https://gitlab.com/apiazza134/colloquio-sns/badges/master/pipeline.svg)

# Colloquio SNS

## Slides
Il PDF più recente si può scaricare [qui](https://gitlab.com/apiazza134/colloquio-sns/-/jobs/artifacts/master/raw/beamer/slides-colloquio.pdf?job=compile_slides).

## Note
Il PDF più recente si può scaricare [qui](https://gitlab.com/apiazza134/colloquio-sns/-/jobs/artifacts/master/raw/note/note-colloquio.pdf?job=compile_note).
