\documentclass[mathserif,9pt]{beamer}
\usepackage{style}

\title[Conformal Bootstrap]{
    The Conformal Bootstrap \\and \\its application to the Ising model in 3d
}
\author[Alessandro Piazza]{
    Alessandro Piazza \\
    \textcolor{gray}{Advisor:} Alessandro Vichi
}
\date[30 aprile 2021]{
    30 aprile 2021
    % Anno Accademico 2020/2021
}
\institute[]{
    Scuola Normale Superiore
}
\titlegraphic{
    \vfill
    \includegraphics[scale=0.5]{img/logo-sns}
}

\begin{document}
\begin{frame}
    \titlepage{}
\end{frame}
\begin{frame}
    \tableofcontents
\end{frame}

\section{Introduction and motivations}
\begin{frame}
    \begin{itemize}
        \item scale invariant system \( \Rightarrow \) no dimensional analysis
        \vfill
        \item scale invariance vs conformal invariance
        \vfill
        \item CFT are very constrained
        \vfill
        \item focus on Green functions (no need for a Lagrangian or Hamiltonian picture)
        \vfill
        \item strongly interacting IR theories \( \Rightarrow \) non-perturbative methods
    \end{itemize}
\end{frame}

\section{Conformal Group}
\begin{frame}
    Setting: \( d \)-dimensional Euclidean manifold (\( d \geq 3 \) with \( g_{\mu\nu}(x) = \eta_{\mu\nu} \))
    \begin{equation}
        g_{\mu\nu} \to g'_{\mu\nu}(x') = \Omega^{2}(x) g_{\mu\nu}(x) \qquad \dpd{{x^{\mu}}}{{{x'}^{\nu}}} = \Omega(x) {R(x)^{\mu}}_{\nu}
    \end{equation}
    \vfill
    Taking \( x' = x + \epsilon(x) \) and \( \Omega(x) = 1 + \omega(x) \) we get the \emph{conformal Killing equation}, the infinitesimal transformations and the generators
    \vfill
    % \begin{equation}
    %     \partial_{\mu} \epsilon_{\nu}(x) + \partial_{\nu} \epsilon_{\mu}(x) = \omega(x) g_{\mu\nu}(x) = \frac{2}{d} g_{\mu\nu}(x) \partial_{\rho} \epsilon^{\rho}(x)
    % \end{equation}
    \begin{equation}
        \begin{cases}
            P_{\mu} = \partial_{\mu}  \\
            L_{\mu\nu} = x_{\nu} \partial_{\mu} - x_{\mu}\partial_{\nu} \\
            D = x^{\mu}\partial_{\mu} \\
            K_{\mu} = 2 (x_{\mu} x^{\nu}) \partial_{\nu} - x^{2} \partial_{\mu}
        \end{cases}
        \quad \xrightarrow[]{finite} \quad
        {x'}^{\mu} =
        \begin{dcases*}
            x^{\mu} + a^{\mu} \\
            {M^{\mu}}_{\nu}x^{\nu} \\
            \lambda x^{\mu}  \\
            \frac{x^{\mu} - c^{\mu} x^{2} }{1 - 2(c \cdot x) + c^{2} x^{2}}
        \end{dcases*}
    \end{equation}
    \vfill
    SCT can be rewritten as \( \dfrac{{x'}^{\mu}}{{x'}^{2}} = \dfrac{x^{\mu}}{x^{2}} - c^{\mu} \)
\end{frame}

\subsection{Representations on states}
\begin{frame}
    \frametitle{\insertsubsectionhead}
    \begin{enumerate}
        \item Algebra is isomorphic to \( \mathfrak{so}(d+1, 1) \) or to \( \mathfrak{so}(d, 2) \) depending on space-time signature
        \vfill
        \item \( \SO(d) \subset \) conformal group \( + \) \( [D, L_{\mu\nu}] = 0 \) \( \Rightarrow \) classify states in terms of irrep of rotations in \( d \) dimensions and scaling dimension
        \vfill
        \item Usually we just write \( \ket{\Delta, \ell} \in \mathcal{H} \) meaning
        \begin{equation}
            D\ket{\Delta, \ell} = \Delta \ket{\Delta, \ell} \qquad L_{\mu\nu}^{2} \ket{\Delta, \ell} = C_{2,\ell} \ket{\Delta, \ell}
        \end{equation}
        \vfill
        \item \( P_{\mu} \)/\( K_{\mu} \) acts as raising/lowering operators wrt \( D \)
        \begin{equation}
            D P_{\mu} \ket{\Delta, \ell} = (\Delta + 1) P_{\mu}\ket{\Delta, \ell} \qquad
            D K_{\mu} \ket{\Delta, \ell} = (\Delta - 1) K_{\mu}\ket{\Delta, \ell}
        \end{equation}
        \vfill
        \item When \( D \) is bounded from below (unitary theories) \( \Rightarrow \) existence of \emph{primary} state
        \begin{equation}
            K_{\mu}\ket{\Delta, \ell}\ped{primary} = 0
        \end{equation}
        which defines an infinite tower of \emph{descendants}
        \begin{equation}
            P_{\mu_{1}}^{m_{1}} \cdots P_{\mu_{n}}^{m_{n}} \ket{\Delta, \ell}\ped{primary} \qquad \Rightarrow \qquad D = \Delta + m_{1} + \cdots + m_{n}
        \end{equation}
    \end{enumerate}
\end{frame}

\section{Correlation Functions}
\begin{frame}
    Strong restriction on \( n \)-point functions of primary fields
    \begin{equation*}
        \begin{multlined}
            \braket{O_{\Delta_{1},\ell_{1}}^{a_{1}}(x_{1}) \cdots O_{\Delta_{n}, \ell_{n}}^{a_{n}}(x_{n})} \\
            \quad =
            \Omega^{\Delta_{1}}(x_{1}') \cdots \Omega^{\Delta_{n}}(x_{n}')
            {\mathcal{R}_{\ell_{1}}(x'_{1})^{a_{1}}}_{b_{1}} \cdots {\mathcal{R}_{\ell_{n}}(x'_{n})^{a_{n}}}_{b_{n}}
            \braket{O_{\Delta_{1},\ell_{1}}^{b_{1}}(x_{1}') \cdots O_{\Delta_{n}, \ell_{n}}^{b_{n}}(x_{n}')}
        \end{multlined}
    \end{equation*}

    CFT is a field theory with rules for computing correlation functions
    \begin{enumerate}
        \item \( 2 \)-point functions are fixed (up to a normalization constant)
        \begin{equation}
            \braket{\phi(x_{1})\phi(x_{2})} = \frac{1}{|x_{1} - x_{2}|^{2\Delta}}\qquad
            \braket{O_{1}^{a}(x_{1}) O_{2}^{b}(x_{2})} = \delta_{O_{1} O_{2}} \frac{I^{ab}(x_{12})}{x_{12}^{2\Delta}}
        \end{equation}
        \item \( 3 \)-point functions are fixed up to global numerical coefficient \( \rightarrow \) \emph{CFT data}
        \item \( n \)-point functions are reduced to \( (n-1) \)-point functions via the \emph{OPE}
        \begin{equation}
            O_{\Delta_{1}, \ell_{2}}^{a}(x_{1}) \times O_{\Delta_{1}, \ell_{2}}^{b}(x_{2}) = \sum_{\Delta, \ell} {C^{ab}}_{c}(x_{12}, \partial_{2}) O^{c}_{\Delta, \ell}(x_{2})
        \end{equation}
    \end{enumerate}
    \vfill
    \( \Rightarrow \) CFT \( = \) \textbf{CFT data} \( + \) \textbf{OPE spectrum} (\( + \) global symmetry group) but not every data and spectrum is consistent!
\end{frame}

\subsection{Crossing Symmetry}
\begin{frame}
    \frametitle{\insertsubsectionhead}
    OPE associativity
    \begin{gather*}
        \left[O_{1}(x_{1}) \times O_{2}(x_{2})\right] \times O_{3}(x_{3}) = O_{1}(x_{1}) \times \left[O_{2}(x_{2}) \times O_{3}(x_{3})\right] \\
        \sum_{O, O'} C_{12 O}(x_{12}, \partial_{2}) C_{O 3 O'}(x_{23}, \partial_{3}) O'(x_{3}) = \sum_{O, O'} C_{23 O}(x_{23}, \partial_{3}) C_{1 O O'}(x_{13}, \partial_{3}) O'(x_{3})
    \end{gather*}
    \vfill
    Take correlator with \( O_{4}(x_{4}) \) and use normalization
    \begin{equation}
        \sum_{O} C_{12 O}(x_{12}, \partial_{2}) C_{O 3 4}(x_{23}, \partial_{3}) h_{O}(x_{34}) = \sum_{O} C_{23 O}(x_{23}, \partial_{3}) C_{1 O 4}(x_{13}, \partial_{3}) h_{O}(x_{34})
    \end{equation}
    \begin{figure}
        \centering
        \includegraphics[scale=1]{img/fig01-bootstrapO}
    \end{figure}
    \vfill
    This is equivalent to
    \begin{equation}
        \braket{\wick{\c1{O}_{1}(x_{1}) \c1{O}_{2}(x_{2}) \c1{O}_{3}(x_{3}) \c1{O}_{4}(x_{4})}} = \braket{\wick{\c2{O}_{1}(x_{1}) \c1{O}_{2}(x_{2}) \c1{O}_{3}(x_{3}) \c2{O}_{4}(x_{4})}}
    \end{equation}
\end{frame}
\subsection{Scalar 4-point function and Crossing Equation}
\begin{frame}
    \frametitle{\insertsubsectionhead}
    \begin{columns}
        \begin{column}{0.55\linewidth}
            Conformal invariant \( 4 \)-point functions have really just 2 dof  \( \rightarrow \) \emph{conformal frame}
            \begin{equation*}
                u = z \bar{z} = \frac{x_{12}^{2} x_{34}^{2}}{x_{13}^{2} x_{24}^{2}} \quad v = (1-z)(1-\bar{z}) = \frac{x_{23}^{2} x_{14}^{2}}{x_{13}^{2} x_{24}^{2}}
            \end{equation*}
        \end{column}
        \begin{column}{0.43\linewidth}
            \centering
            \input{img/conformal-frame}
        \end{column}
    \end{columns}

    \vfill
    \begin{equation}
        \braket{\phi(x_{1}) \phi(x_{2}) \phi(x_{3}) \phi(x_{4})} = \frac{g(u, v)}{x_{12}^{2\Delta_{\phi}} x_{34}^{2\Delta_{\phi}}}
    \end{equation}
    Crossing symmetry is
    \begin{equation}
        \braket{\wick{\c1{\phi}(x_{1}) \c1{\phi}(x_{2}) \c1{\phi}(x_{3}) \c1{\phi}(x_{4})}} = \braket{\wick{\c2{\phi}(x_{1}) \c1{\phi}(x_{2}) \c1{\phi}(x_{3}) \c2{\phi}(x_{4})}}
    \end{equation}
    \vfill
    Equivalent to the invariance under \( 2 \leftrightarrow 4 \) or \( 1 \leftrightarrow 3 \), i.e. under \( u \leftrightarrow v \)
    \begin{equation}
        \frac{g(v, u)}{x_{14}^{2\Delta_{\phi}} x_{23}^{2\Delta_{\phi}}} = \frac{g(u, v)}{x_{12}^{2\Delta_{\phi}} x_{34}^{2\Delta_{\phi}}}
        \qquad \Rightarrow \qquad
        \boxed{
            {v}^{\Delta_{\phi}} g(u, v) - u^{\Delta_{\phi}} g(v, u) = 0
        }
    \end{equation}
\end{frame}


\section{Conformal Bootstrap}
\begin{frame}
    \begin{equation}
        \braket{\phi(x_{1}) \phi(x_{2}) \phi(x_{3}) \phi(x_{4})} =  \frac{1}{x_{12}^{2\Delta_{\phi}} x_{34}^{2\Delta_{\phi}}} \sum_{(\Delta, \ell) \in I} \lambda_{\phi\phi O_{\Delta, \ell}}^{2} g_{\Delta, \ell}(u, v)
    \end{equation}
    \vfill
    \begin{itemize}
        \item \( I \) is the CFT spectrum, i.e. the set of primary operators appearing in the OPE \( \phi \times \phi \)
        \vfill
        \item the \emph{conformal blocks} \( g_{\Delta, \ell}(u, v) \) are known exactly or approximately
        \vfill
        \item CFT data \( \{\lambda_{\phi\phi O_{\Delta, \ell}}\} \) is unknown in a unitary theory \( \lambda_{\phi\phi O_{\Delta, \ell}}^{2} \geq 0 \), \( \lambda_{\phi\phi \mathrm{Id}} = 1 \)
        \vfill
        \item CFT spectrum (except for \( \mathrm{Id} \)) must satisfy unitary bounds
        \begin{equation}
            \Delta \geq \dfrac{d-2}{2} \quad\text{for }\ell = 0 \qquad \Delta \geq d - 2 + \ell \quad\text{for } \ell \geq 1
        \end{equation}
        \vfill
        \item conformal blocks must satisfy the \emph{crossing equation}
        \begin{equation}
            \boxed{
                \sum_{\Delta, \ell} \lambda_{\phi\phi O_{\Delta, \ell}}^{2} F_{\Delta, \ell}(u, v) = 0}
        \end{equation}
        where
        \begin{equation}
            F_{\Delta, \ell}(u, v) = {v}^{\Delta_{\phi}} g_{\Delta, \ell}(u, v) - u^{\Delta_{\phi}} g_{\Delta, \ell}(v, u)
        \end{equation}
    \end{itemize}
\end{frame}
\subsection{Bootstrap philosophy}
\begin{frame}
    \frametitle{\insertsubsectionhead}
    \begin{block}{The problem}
        Given a family of functions \( \{f_{\alpha}(x)\} \) parameterized by \( \alpha \in I \), find bounds on \( I \) so that it's compatible with the constraint
        \begin{equation}
            \label{eq:boot-prelude}
            \sum_{\alpha \in I} p_{\alpha} f_{\alpha}(x) = 0 \qquad \forall x, \{p_{\alpha} \geq 0\}, p_{\bar{\alpha}} > 0
        \end{equation}
    \end{block}
    \vfill
    \begin{enumerate}
        \item \textbf{semi-definite programming} \( \rightarrow \) more cumbersome but more efficient
        \vfill

        Based on \emph{dual formulation}: assume a lower bound \( \alpha\ped{B} \) for \( I \) and look for a linear functional \( v \) such that \( v(f_{\alpha}) \geq 0 \) and \( v(f_{\bar{\alpha}}) > 0 \)
        \begin{itemize}
            \item can find such a functional \( \Rightarrow \) inconsistent with~\eqref{eq:boot-prelude} and we must lower the bound
            % \begin{equation}
            %     0 = \sum_{\alpha \in I} p_{\alpha} v(f_{\alpha}) \geq p_{0} v(f_{0}) > 0
            % \end{equation}
            \item cannot find such a functional \( \Rightarrow \) consistent and we try to raise the bound
        \end{itemize}
        Then proceed by bisection
        \vfill

        \item \textbf{linear programming} \( \rightarrow \) easy to understand but not very efficient
    \end{enumerate}
\end{frame}
\subsection{Solving via Linear Programming}
\begin{frame}
    \frametitle{\insertsubsectionhead}
    % Isolate the identity operator and normalize
    \begin{equation}
        \sum_{(\Delta, \ell) \in I} \frac{\lambda_{\phi\phi O_{\Delta, \ell}}^{2} F_{\Delta, \ell}(u, v)}{\lambda_{\phi\phi \mathrm{Id}}^{2} F_{0, 0}(u, v)} = - 1
        \qquad \rightarrow \qquad
        \sum_{(\Delta, \ell) \in I} p_{\Delta, \ell} H_{\Delta, \ell}(u, v) = -1
    \end{equation}
    \vfill
    Need to deal with multiple infinities
    \begin{itemize}
        \item functional constraint \( \Rightarrow \) choose a point \( (u_{0}, v_{0}) \) and Taylor expand up to a cutoff \( \Lambda \)
        \begin{equation}
            \sum_{(\Delta, \ell) \in I} p_{\Delta, \ell} \dmd{}{n+m}{u}{n}{v}{m} H_{\Delta, \ell}(u_{0}, v_{0}) = -\delta_{n0} \delta_{m0} \qquad \forall (n, m) \leq \Lambda
        \end{equation}
        \item CFT spectrum is generically unbounded \( \Rightarrow \) hard cutoff \( \Delta\ped{max}, \ell\ped{max} \)
        \item \( \Delta \) is a continuous parameter \( \Rightarrow \) discretize \( [\min\{\Delta\ped{B}, \Delta\ped{UB}\}, \Delta\ped{max}] \)
    \end{itemize}
    \vfill
    Then we just check if, for a trial regularized spectrum \( I' \), a huge linear system has solution
    \begin{equation}
        \begin{pmatrix}
            H_{\Delta_{1}, \ell_{1}}^{(0, 0)} & H_{\Delta_{2}, \ell_{2}}^{(0, 0)} & \cdots \\[3pt]
            H_{\Delta_{1}, \ell_{1}}^{(1, 0)} & H_{\Delta_{2}, \ell_{2}}^{(1, 0)} & \cdots \\
            \vdots & \vdots & \ddots
        \end{pmatrix}
        \begin{pmatrix}
            p_{\Delta_{1}, \ell_{1}} \\
            p_{\Delta_{2}, \ell_{2}} \\
            \vdots
        \end{pmatrix}
        =
        \begin{pmatrix}
            -1 \\
            0 \\
            \vdots
        \end{pmatrix}
    \end{equation}
\end{frame}

\section{\( 3d \) Ising Model}
\begin{frame}
    \begin{itemize}
        % \item Ising Model in \( 3d \) at the critical point is a CFT with a \( \mathbb{Z}_{2} \) symmetry
        \item We have in mind the IR fixed point of the UV theory
        \begin{equation}
            S = \int \mathrm{d}^{3}{\vec{x}} \left[\frac{1}{2} (\partial_{\mu} \sigma(x))^{2} + \frac{\lambda}{4!} \sigma^{4}(x)\right] \qquad \text{or} \qquad H = -J \sum_{\braket{i, j}} s_{i} s_{j}
        \end{equation}
        \item Under RG flow fields may develop anomalous dimensions
        \begin{equation}
            \Delta_{\sigma}\ap{UV} = 0.5 \to \Delta_{\sigma} \simeq 0.518 \qquad
            \Delta_{\sigma^{2}}\ap{UV} = 1 \to \Delta_{\epsilon} \simeq 1.41
        \end{equation}

        \item We study the bounds on scaling dimension of scalar operators which may appear in the 4 point function \( \braket{\sigma \sigma \sigma \sigma} \)
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[scale=0.7]{img/fig07-3dIsingkink}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Bootstrapping the \( 3d \) Ising island}
    \begin{itemize}
        \item Use mixed correlation functions \( \braket{\sigma \sigma \sigma \sigma}, \braket{\epsilon \epsilon \epsilon \epsilon}, \braket{\sigma \sigma \epsilon \epsilon}\)
        \item Input more physical assumptions: \emph{there exists only two relevant scalar operators}
        \begin{equation}
            S \to S + h \int\mathrm{d}^{3}\vec{x} \, \sigma(x) + \frac{m^{2}}{2} \int\mathrm{d}^{3}\vec{x} \, \sigma^{2}(x)
        \end{equation}
        This corresponds to the OPE expansion
        \begin{equation}
            \begin{cases}
                \sigma \times \sigma \sim 1 + \epsilon + (\text{scalar operators with } \Delta \geq 3) + (\ell > 0) \\
                \epsilon \times \epsilon \sim 1 + \epsilon + (\text{scalar operators with } \Delta \geq 3) + (\ell > 0) \\
                \sigma \times \epsilon \sim \sigma + (\text{scalar operators with } \Delta \geq 3) + (\ell > 0)
            \end{cases}
        \end{equation}
    \end{itemize}

    \begin{columns}
        \begin{column}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{img/fig13-mixed-differentgaps}
        \end{column}
        \begin{column}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{img/fig15-IsingIsland.pdf}
        \end{column}
    \end{columns}
    \hfill
\end{frame}

\begin{frame}
    \frametitle{References}
    \nocite{*}

    \printbibliography{}
\end{frame}


% \setbeamertemplate{page number in head/foot}{\phantom{ 0/0}}
\appendix
\section{Representations on fields}
\begin{frame}
    \begin{enumerate}
        \item As for states we classify fields at the origin by their scaling dimension and the rotation under \( \SO(d) \)
        \begin{equation}
            [D, O_{\Delta, \ell}^{a}(0)] = \Delta O_{\Delta, \ell}^{a}(0) \qquad
            [L_{\mu\nu}, O_{\Delta, \ell}^{a}(0)] = {(M_{\mu\nu})^{a}}_{b} O^{b}_{\Delta, \ell}(0)
        \end{equation}
        \item Use \( O_{\Delta, \ell}(x) = e^{x^{\mu} P_{\mu}} O_{\Delta, \ell}(0) e^{-x^{\mu} P_{\mu}} \) and commutation relations to find the action of conformal algebra on \( O_{\Delta, \ell}(x) \) implemented by differential operators, e.g.
        \begin{gather*}
            [D, O_{\Delta, \ell}(x)] = (x^{\mu}\partial_{\mu} + \Delta) O_{\Delta, \ell}(x) \\
            [L_{\mu\nu}, O_{\Delta, \ell}^{a}(x)] = \left[{(M_{\mu\nu})^{a}}_{b} + (x_{\nu}\partial_{\mu} - x_{\mu} \partial_{\nu}) \delta^{a}_{b}\right] O_{\Delta, \ell}^{b}(x)
        \end{gather*}

        \item General transformation law
        \begin{equation}
            \dpd{{{x'}^{\mu}}}{{x^{\nu}}} = \Omega(x) {R(x)^{\mu}}_{\nu} \quad \rightarrow \quad
            U O_{\Delta,\ell}^{a}(x) U^{-1} = \Omega^{\Delta}(x') {[D(R(x'))]^{a}}_{b} O^{b}_{\Delta, \ell}(x')
        \end{equation}
        For example, transformation under dilation \( e^{\lambda D} O_{\Delta,\ell}(x) e^{-\lambda D} = \lambda^{\Delta} O_{\Delta, \ell}(\lambda x) \)
        \item We focus on scalars \( \phi_{\Delta}(x) \) and traceless symmetric tensors \( O_{\Delta, \ell}^{\mu_{1} \cdots \mu_{\ell}}(x) \)
        \item Analogously, if \( D \) is bounded from below we have \emph{primary} operators and their descendants
        \begin{equation}
            [K_{\mu}, O\ped{primary}(0)] = 0
        \end{equation}
    \end{enumerate}
\end{frame}
\section{Unitary and Local CFT}
\begin{frame}
    \begin{itemize}
        \item \textbf{Unitary}:
        % : in Euclidean field theory the hermiticity is replaced by the \emph{reflection positivity condition}
        % \begin{equation}
        %     [O^{\mu_{1} \cdots \mu_{\ell}}(t, \vec{x})]^{\dagger} = {\theta^{\mu_{1}}}_{\nu_{1}} \cdots {\theta^{\mu_{\ell}}}_{\nu_{\ell}} O^{\nu_{1} \cdots \nu_{\ell}}(-t, \vec{x}) \qquad
        %     {\theta^{\mu}}_{\nu} = \delta^{\mu}_{\nu} - 2 \delta^{\mu}_{0} \delta^{0}_{\nu}
        % \end{equation}
        % Implications
        \begin{enumerate}
            \item \( P_{\mu}^{\dagger} = K_{\mu} \) \( \Rightarrow \) unitarity bounds on the spectrum of \( D \)
            \begin{equation}
                \Delta \geq
                \begin{cases}
                    (d-2)/2 & \ell = 0 \text{ except for identity operator} \\
                    \ell + d - 2 & \ell \geq 1
                \end{cases}
            \end{equation}
            \item CFT data is real
        \end{enumerate}
        \vfill
        \item \textbf{Local}: CFT with a conserved symmetric traceless energy-momentum tensor \( T_{\mu\nu}(x) \), then the generators are given by
        \begin{equation}
            Q(\Sigma) = - \int_{\Sigma} \epsilon_{\nu}(x) T^{\mu\nu}(x) \, \dif{S_{\mu}}
        \end{equation}
        where \( \epsilon_{\mu}(x) \) is a solution to the conformal Killing equation, meaning that
        \begin{equation}
            \braket{Q(\Sigma) \Phi(x_{1}) \cdots \Phi(x_{n})} = \epsilon^{\mu}(x_{1}) \Braket{\partial^{1}_{\mu} \Phi(x_{1}) \Phi(x_{2})\cdots \Phi(x_{n})}
        \end{equation}
        for every surface \( \Sigma \) surrounding just \( x_{1} \).
    \end{itemize}
    \vfill
\end{frame}

\section{Scalar 2-point function}
\begin{frame}
    \begin{align*}
      \SO(d)\text{ invariance}: &\qquad
                                  \braket{\phi_{1}(x_{1}) \phi_{2}(x_{2})} = f(x_{12}) \qquad x_{12} = |x_{1} - x_{2}| \\
      \text{Scale invariance}: &\qquad
                                 f(\lambda x_{12}) = \braket{\phi_{1}(\lambda x_{1}) \phi_{2}(\lambda x_{2})} = \lambda^{-(\Delta_{1} + \Delta_{2})} f(x_{12})
    \end{align*}
    where we used \( \phi(\lambda x) = \lambda^{-\Delta} (e^{\lambda D} \phi_{\Delta}(x) e^{-\lambda D}) \) and the invariance of correlation functions
    \begin{equation}
        \Rightarrow \quad \braket{\phi_{1}(x_{1}) \phi_{2}(x_{2})} = \frac{c_{\Delta_{1} \Delta_{2}}}{x_{12}^{\Delta_{1} + \Delta_{2}}}
    \end{equation}
    \vfill
    Using the transformation laws
    \begin{gather}
        \braket{\phi_{1}(x_{1}) \phi_{2}(x_{2})} = \Omega^{\Delta_{1}}(x_{1}') \Omega^{\Delta_{2}}(x_{2}') \braket{\phi_{1}(x_{1}') \phi_{2}(x_{2}')} \\
        (x_{1} - x_{2})^{2} = \frac{(x'_{1} - x'_{2})^{2}}{\Omega(x_{1}') \Omega(x_{2}')}
    \end{gather}
    invariance under the full conformal group imposes \( c_{\Delta_{1} \Delta_{2}} = c \delta_{\Delta_{1} \Delta_{2}} \). \vfill

    The only non-vanishing scalar 2 point functions are between identical scalars
    \begin{equation}
        \braket{\phi(x_{1}) \phi(x_{2})} = \frac{c}{x_{12}^{2\Delta}} \xrightarrow{norm.} \frac{1}{x_{12}^{2\Delta}}
    \end{equation}
\end{frame}


\section{Scalar 3-point function}
\begin{frame}
    Conformal invariance imposes
    \begin{gather}
        \braket{\phi_{1}(x_{1}) \phi_{2}(x_{2}) O_{\Delta, \ell}^{\mu_{1} \cdots \mu_{\ell}}(x_{3})} = \frac{\lambda_{\phi_1 \phi_2 O}}{x_{12}^{\Delta_{12O} + \ell} x_{23}^{\Delta_{1O2} -\ell} x_{31}^{\Delta_{O12} - \ell}} \left(Z^{\mu_{1}}_{123} \cdots Z^{\mu_{\ell}}_{123} - \text{traces}\right) \\
        \Delta_{ijk} = \Delta_{i} + \Delta_{j} - \Delta_{k} \qquad Z^{\mu}_{ijk} = \frac{x_{ik}^{\mu}}{x_{ik}^{2}} - \frac{x_{jk}^{\mu}}{x_{jk}^{2}} \nonumber
    \end{gather}
    \( \lambda_{\phi_1 \phi_2 O} \) is characteristic of the CFT once the scalar 2-point function normalization is fixed
    \vfill
    Consistency with OPE
    \begin{align*}
      \braket{\phi_{1}(x_{1}) \phi_{2}(x_{2}) O_{\Delta, \ell}^{a}(x_{3})} &= \sum_{\Delta', \ell'} C_{b}^{\phi_1 \phi_2 O'}(x_{12}, \partial_{2}) \braket{ O_{\Delta', \ell'}^{b}(x_{2})  O_{\Delta, \ell}^{a}(x_{3})} \\
                                                                           & = C_{b}^{\phi_1 \phi_2 O}(x_{12}, \partial_{2}) \frac{I^{ba}(x_{23})}{x_{23}^{2\Delta}}
    \end{align*}
    Hence
    \begin{equation}
        C_{b}^{\phi_1 \phi_2 O}(x_{12}, \partial_{2}) \frac{I^{ba}(x_{23})}{x_{23}^{2\Delta}} = \frac{\lambda_{\phi_{1} \phi_{2} O'}  Z^{a}_{123}}{x_{12}^{\Delta_{12O} + \ell} x_{23}^{\Delta_{1O2} -\ell} x_{31}^{\Delta_{O12} - \ell}}
    \end{equation}
    Hence we usually factor the CFT data explicitly inside the OPE
    \begin{equation}
        C_{O_{1} O_{2} O}(x_{12}, \partial_{2}) \quad\to\quad
        \lambda_{O_{1} O_{2} O} C_{O_{1} O_{2} O}(x_{12}, \partial_{2})
    \end{equation}
\end{frame}


\section{Conformal Blocks}
\begin{frame}
    Primary states and their descendants form a complete set of states, hence
    \begin{equation}
        \braket{\phi(x_{1})\phi(x_{2})\phi(x_{3})\phi(x_{4})} = \sum_{\Delta, \ell} \sum_{m, s} \frac{\braket{0 | \phi(x_{4}) \phi(x_{3}) | \Delta + m, s} \braket{\Delta + m, s | \phi(x_{2}) \phi(x_{1}) |0}}{\braket{\Delta + m, s | \Delta + m, s}}
    \end{equation}
    Use OPE to expand the 4-point function of identical scalars
    \begin{equation}
        \braket{\phi(x_{1}) \phi(x_{2}) \phi(x_{3}) \phi(x_{4})} = \sum_{\Delta, \ell} \lambda_{\phi\phi O}^{2} C_{a}^{\phi\phi O}(x_{12}, \partial_{2}) C_{b}^{\phi\phi O}(x_{34}, \partial_{4}) \frac{I^{ab}(x_{24})}{x_{24}^{2\Delta}}
    \end{equation}
    Therefore we can expand
    \begin{equation}
        g(u, v) = \sum_{\Delta, \ell} \lambda_{\phi\phi O_{\Delta, \ell}}^{2} g_{\Delta, \ell}(u, v)
    \end{equation}
    where \( g_{\Delta, \ell} \) are called \emph{conformal blocks}
    \begin{equation}
        g_{\Delta, \ell}(u, v) = \frac{x_{12}^{2\Delta_{\phi}} x_{34}^{2\Delta_{\phi}}}{\lambda^{2}_{\phi\phi O_{\Delta, \ell}}} \sum_{m, s} \frac{\braket{0 | \phi(x_{4}) \phi(x_{3}) | \Delta + m, s} \braket{\Delta + m, s | \phi(x_{2}) \phi(x_{1}) |0}}{\braket{\Delta + m, s | \Delta + m, s}}
    \end{equation}
    which are fixed by the conformal symmetry
\end{frame}

\section{Conformal Casimir Equation}
\begin{frame}
    The conformal commutator algebra is
    \begin{gather*}
        [L_{\mu\nu}, L_{\rho\sigma}] = \eta_{\nu\rho} L_{\mu\sigma} - \eta_{\mu\rho} L_{\nu\sigma} + \eta_{\nu\sigma} L_{\rho\mu} - \eta_{\mu\sigma} L_{\rho\nu} \\
        [L_{\mu\nu}, P_{\rho}] = \eta_{\nu\rho} P_{\mu} - \eta_{\mu\rho} P_{\nu} \qquad
        [L_{\mu\nu}, K_{\rho}] = \eta_{\nu\rho} K_{\mu} - \eta_{\mu\rho} K_{\nu} \qquad
        [L_{\mu\nu}, D] = 0 \\
        [D, P_{\mu}] = P_{\mu} \qquad
        [D, K_{\mu}] = -K_{\mu}  \qquad
        [K_{\mu}, P_{\nu}] = 2(\eta_{\mu\nu} D - L_{\mu\nu})
    \end{gather*}
    Setting
    \begin{equation}
        J_{\mu\nu} = L_{\mu\nu} \qquad J_{-1 0} = D \qquad J_{0\mu} = \frac{P_{\mu} + K_{\mu}}{2} \qquad J_{-1\mu} = \frac{P_{\mu} - K_{\mu}}{2}
    \end{equation}
    we get
    \begin{equation}
        [J_{ab}, J_{cd}] = \eta_{bc} J_{ad} - \eta_{ac} J_{bd} + \eta_{bd} J_{ac} - \eta_{ad} J_{bc}
    \end{equation}
    where \( a, b \in \{-1, 0, \mu\}, \mu \in \{1, \cdots, d\} \) and \( \eta_{ab} = \mathrm{diag}(-1, 1, 1, \cdots, 1) \).
    \vfill
    The conformal Casimir is then
    \begin{equation}
        C = -\frac{1}{2} J_{ab} J^{ab} \qquad C \ket{\Delta, \ell} = c_{\Delta, \ell} \ket{\Delta, \ell} = [\Delta (\Delta - d) + \ell (\ell + d - 2)] \ket{\Delta, \ell}
    \end{equation}

\end{frame}
\begin{frame}
    Now notice that
    \begin{equation}
        J_{ab} \phi(x_{1}) \phi(x_{2}) \ket{0} = [J_{ab}, \phi(x_{1})] \phi(x_{2})\ket{0} + \phi(x_{1}) [J_{ab}, \phi(x_{2})] \ket{0}
    \end{equation}
    Let \( [J_{ab}, \phi(x)] = \mathcal{J}_{ab} \phi(x) \) where \( \mathcal{J}_{ab} \) is a differential operator, then
    \begin{equation}
        J_{ab} \phi(x_{1}) \phi(x_{2}) \ket{0} = (\mathcal{J}_{ab, 1} + \mathcal{J}_{ab, 2}) \phi(x_{1})\phi(x_{2}) \ket{0}
    \end{equation}
    Hence
    \begin{equation*}
        c_{\Delta, \ell} \braket{\Delta + m, s | \phi(x_{2}) \phi(x_{1}) |0}
        = - \frac{1}{2} (\mathcal{J}_{ab, 1} + \mathcal{J}_{ab, 2}) (\mathcal{J}^{ab}_{1} + \mathcal{J}^{ab}_{2})  \braket{\Delta + m, s | \phi(x_{2}) \phi(x_{1}) |0}
    \end{equation*}
    The differential equation is
    \begin{equation}
        \mathcal{D}_{12} g_{\Delta, \ell}(u, v) = \left[
            \frac{1}{2} (\mathcal{J}_{ab, 1} + \mathcal{J}_{ab, 2}) (\mathcal{J}^{ab}_{1} + \mathcal{J}^{ab}_{2}) + c_{\Delta, \ell}
        \right] x_{12}^{-2\Delta_{\phi}} x_{34}^{-2\Delta_{\phi}} g_{\Delta, \ell}(u, v) = 0
    \end{equation}
    \begin{itemize}
        \item exact solutions in even dimensions in terms of hypergeometric functions
        \begin{gather}
            g_{\Delta, \ell}^{(d = 4)}(z, \bar{z}) = \frac{z \bar{z}}{z - \bar{z}}\left[\kappa_{\Delta + \ell}(z) \kappa_{\Delta -\ell-2}(\bar{z}) -  \kappa_{\Delta -\ell-2}(z) \kappa_{\Delta + \ell}(\bar{z})\right] \\
            \kappa_{\beta}(x) = x^{\beta/2} F_{2,1}(\beta/2, \beta/2, \beta, x) \nonumber
        \end{gather}
        \item approximate solutions in odd dimensions as power series and/or partial fractions expansion
    \end{itemize}
\end{frame}

% \section{CFT with global symmetry}
% \begin{frame}
%     \textcolor{red}{TODO}
% \end{frame}


\section{Solving via Semi-definite Programming}
\begin{frame}
    \begin{equation}
        g_{\Delta, \ell}(r, \theta) = \frac{x_{12}^{2\Delta_{\phi}} x_{34}^{2\Delta_{\phi}}}{\lambda^{2}_{\phi\phi O_{\Delta, \ell}}} \sum_{m, s} \frac{\braket{0 | \phi(x_{4}) \phi(x_{3}) | \Delta + m, s} \braket{\Delta + m, s | \phi(x_{2}) \phi(x_{1}) |0}}{\braket{\Delta + m, s | \Delta + m, s}}
    \end{equation}
    \textbf{Key observation}: for special values of \( \Delta = \Delta^{\star}_{A} \), there exists a descendant state \( \ket{\Delta^{\star}_{A} + n_{A}, s_{A}} \) which becomes a primary \( \Rightarrow \) \(  \ket{\Delta^{\star}_{A} + n_{A}, s_{A}} \) is a \emph{null} state

    \vfill

    Analytic properties in \( \Delta \) con be classified and we can write a recursive relation
    \begin{equation}
        g_{\Delta, \ell}(r, \theta) = r^{\Delta}\left[h_{\infty, \ell}(r, \theta) + \sum_{I} \sum_{m \in B_{I}} \frac{c_{I, m}}{\Delta - \Delta_{I, m}} h_{\Delta_{I, m} + m, \ell_{I, m}}(r, \theta)\right]
    \end{equation}
    with \( h_{\infty, \ell}(u, v) \) an entire function solution of the CCE for \( \Delta \to +\infty \).

    \vfill

    Hence we can always rewrite
    \begin{equation}
        \dmd{}{n+m}{z}{n}{\bar{z}}{m} F_{\Delta, \ell}(r = r\ped{c}, \eta = 0) = \chi_{\ell}(\Delta) P_{\ell}^{(n, m)}(\Delta) \qquad \chi_{\ell}(\Delta) = \frac{r\ped{c}^{\Delta}}{\displaystyle\prod_{I} \prod_{m \in B_{I}}(\Delta - \Delta_{I, m})}
    \end{equation}
    where \( \chi_{\ell}(\Delta) \geq 0 \) since \( \Delta_{I, m} \leq \Delta\ped{UB} \) and \( P_{\ell}^{(n, m)}(\Delta) \) a polynomial (infinite if \( \ell \) unbounded).
\end{frame}

\begin{frame}
    \begin{block}{Polynomial matrix problem}
        Given \( b \in \R^{W} \) and a set of symmetric polynomial matrices
        \begin{equation}
            M_{j}^{w}(x) =
            \begin{pmatrix}
                P^{w}_{j, 11}(x) & \cdots & P^{w}_{j, 1 m_{j}}(x) \\
                \vdots & \ddots & \vdots \\
                P^{w}_{j, m_{j} 1}(x) & \cdots & P^{w}_{j, m_{j} m_{j}}(x)
            \end{pmatrix}
            \qquad w \in \{0, \ldots, W\}, j \in \{1, \ldots, J\}
        \end{equation}
        \begin{align}
          \text{maximize} & \qquad b^{0} + \sum_{w=1}^{W} b_{w} y_{w} \\
          \text{s.t.} & \qquad M_{j}^{0}(x) + \sum_{w=1}^{W} y_{w} M^{w}_{j}(x) \succeq 0 \qquad \forall x \geq 0 \nonumber
        \end{align}
    \end{block}
    \vfill
    Simplest case of conformal bootstrap for 4 identical scalars
    \begin{itemize}
        \item \( y_{w} \rightarrow v_{n,m} \) components of the linear functional on the space of \( \{F_{\Delta, \ell}(r, \theta) \} \) regularized to the space of the first \( \Lambda \) derivatives at the crossing symmetric point
        \item \( m_{j} = 1 \Rightarrow M_{j}^{w}(x) = P^{w}_{j, 11}(x) \rightarrow P_{\ell}^{(n, m)}(\Delta\ped{B}^{\ell} + x)\)
        \item \( W \to \Lambda, J \to \ell\ped{max} \)
    \end{itemize}
    More powerful because it can handle continuous \( \Delta \), mixed-correlators, higher spin, \ldots
\end{frame}
\end{document}