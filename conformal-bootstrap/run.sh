#!/usr/bin/env bash

EXEC=$@

# set -e

# DOCKER_COMPOSE=/local/scratch/alepiazza/colloquio-sns/sdpb-docker/docker-compose.yml
# DOCKER_ID=$(docker-compose -f ${DOCKER_COMPOSE} ps -q)

# if [ -z $DOCKER_ID ]; then
#     docker-compose -f ${DOCKER_COMPOSE} up -d
#     DOCKER_ID=$(docker-compose -f ${DOCKER_COMPOSE} ps -q)
# fi

DOCKER_ID=sdpb-docker_sdpb_1

# set -o xtrace
docker exec ${DOCKER_ID} bash -c "${EXEC}"
