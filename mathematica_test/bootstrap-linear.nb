(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     22615,        552]
NotebookOptionsPosition[     21200,        525]
NotebookOutlinePosition[     21718,        544]
CellTagsIndexPosition[     21675,        541]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[StyleBox["2D conformal bootstrap with linear programming", \
"Section"]], "Text",
 CellChangeTimes->{{3.8274860881169987`*^9, 
  3.827486112364792*^9}},ExpressionUUID->"d6a2dfa9-7e02-45de-8e87-\
e4e7e6321cfd"],

Cell[TextData[StyleBox["Global parameters", "Subsection"]], "Text",
 CellChangeTimes->{{3.827486185252058*^9, 
  3.827486188731893*^9}},ExpressionUUID->"96fd1152-ecda-493d-adbf-\
11ee484be6d7"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"d", " ", "=", " ", "2"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalLambda]", " ", "=", " ", "3"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"derivative", " ", "cutoff"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"lmax", " ", "=", " ", "4"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"spin", " ", "cutoff"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalDelta]max", " ", "=", " ", "6"}], ";"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"dimension", " ", "cutoff"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalDelta]\[Phi]", " ", "=", " ", 
    RowBox[{"1", "/", "8"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "dimension", " ", "of", " ", "the", " ", "scalar", " ", "\[Phi]", " ", 
     "in", " ", "the", " ", "4"}], "-", 
    RowBox[{"point", " ", "function"}]}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalDelta]\[Epsilon]", " ", "=", " ", "4"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "initial", " ", "guess", " ", "for", " ", "scalar", " ", "dimension", " ", 
    "gap"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalDelta]UB", " ", "=", " ", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"d", "-", "2"}], ")"}], "/", "2"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "unitarian", " ", "bound", " ", "for", " ", "scalar", " ", "operators"}], 
   " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"\[Delta]", "=", " ", "0.001"}], " ", ";"}], 
  RowBox[{"(*", " ", 
   RowBox[{"spacing", " ", "for", " ", "scalar", " ", "dimension"}], " ", 
   "*)"}]}]}], "Input",
 CellChangeTimes->{{3.82748607090659*^9, 3.8274860736773157`*^9}, {
   3.827486281312511*^9, 3.827486281539773*^9}, {3.827488349289878*^9, 
   3.8274883515288877`*^9}, {3.827488528894248*^9, 3.827488542054105*^9}, {
   3.8274885853821993`*^9, 3.827488589109717*^9}, {3.827489385944322*^9, 
   3.827489390830228*^9}, 3.827489722406267*^9, {3.827490144098835*^9, 
   3.827490197653068*^9}, {3.827491374463129*^9, 3.827491374859621*^9}, {
   3.827493970698966*^9, 3.827493971387979*^9}, {3.827504943910357*^9, 
   3.827505003704691*^9}, {3.8275053310369*^9, 3.827505333756844*^9}, {
   3.827505881566799*^9, 3.8275060392385483`*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"390af4cf-58a6-44f4-8645-64765d5e1a6f"],

Cell[TextData[StyleBox["Conformal blocks", "Subsection"]], "Text",
 CellChangeTimes->{{3.8274862012677517`*^9, 
  3.827486204899762*^9}},ExpressionUUID->"72bd05e2-926f-4fe8-94ff-\
2848935fc4f4"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"\[Kappa]", "[", "\[Beta]_", "]"}], "[", "x_", "]"}], " ", ":=", 
  " ", 
  RowBox[{
   SuperscriptBox["x", 
    RowBox[{"\[Beta]", "/", "2"}]], " ", 
   RowBox[{"Hypergeometric2F1", "[", 
    RowBox[{
     FractionBox["\[Beta]", "2"], ",", " ", 
     FractionBox["\[Beta]", "2"], ",", " ", "\[Beta]", ",", " ", "x"}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"g2", "[", 
     RowBox[{"\[CapitalDelta]_", ",", " ", "l_"}], "]"}], "[", 
    RowBox[{"z_", ",", " ", "zb_"}], "]"}], " ", ":=", 
   RowBox[{
    FractionBox[
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"-", "2"}], ")"}], "l"], 
     RowBox[{"1", "+", 
      RowBox[{"KroneckerDelta", "[", 
       RowBox[{"0", ",", " ", "l"}], "]"}]}]], 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"\[Kappa]", "[", 
         RowBox[{"\[CapitalDelta]", " ", "+", "l"}], "]"}], "[", "z", "]"}], 
       RowBox[{
        RowBox[{"\[Kappa]", "[", 
         RowBox[{"\[CapitalDelta]", " ", "-", "l"}], "]"}], "[", "zb", 
        "]"}]}], " ", "+", " ", 
      RowBox[{
       RowBox[{
        RowBox[{"\[Kappa]", "[", 
         RowBox[{"\[CapitalDelta]", " ", "-", "l"}], "]"}], "[", "z", "]"}], 
       RowBox[{
        RowBox[{"\[Kappa]", "[", 
         RowBox[{"\[CapitalDelta]", " ", "+", "l"}], "]"}], "[", "zb", 
        "]"}]}]}], ")"}]}]}], " "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"F2", "[", 
   RowBox[{
   "\[CapitalDelta]_", ",", " ", "l_", ",", " ", "\[CapitalDelta]\[Phi]_", 
    ",", " ", "z_", ",", " ", "zb_"}], "]"}], " ", ":=", " ", 
  FractionBox[
   RowBox[{
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "z"}], ")"}], 
        RowBox[{"(", 
         RowBox[{"1", "-", "zb"}], ")"}]}], ")"}], "\[CapitalDelta]\[Phi]"], 
     " ", 
     RowBox[{
      RowBox[{"g2", "[", 
       RowBox[{"\[CapitalDelta]", ",", " ", "l"}], "]"}], "[", 
      RowBox[{"z", ",", " ", "zb"}], "]"}]}], " ", "-", " ", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"z", " ", "zb"}], ")"}], "\[CapitalDelta]\[Phi]"], 
     RowBox[{
      RowBox[{"g2", "[", 
       RowBox[{"\[CapitalDelta]", ",", " ", "l"}], "]"}], "[", 
      RowBox[{
       RowBox[{"1", "-", "z"}], ",", " ", 
       RowBox[{"1", "-", "zb"}]}], "]"}]}]}], 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", "z"}], ")"}], 
       RowBox[{"(", 
        RowBox[{"1", "-", "zb"}], ")"}]}], ")"}], "\[CapitalDelta]\[Phi]"], 
    " ", "-", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"z", " ", "zb"}], ")"}], 
     "\[CapitalDelta]\[Phi]"]}]]}]}], "Input",
 CellChangeTimes->{{3.8273135975101748`*^9, 3.8273136008539886`*^9}, {
   3.827314738737167*^9, 3.827314744673646*^9}, {3.827316505321939*^9, 
   3.827316505903818*^9}, {3.8273786902682867`*^9, 3.827378715974257*^9}, {
   3.8273806567402687`*^9, 3.8273806581061783`*^9}, {3.827380823336266*^9, 
   3.827380829958798*^9}, {3.827380922203929*^9, 3.8273809245056973`*^9}, {
   3.827381498049489*^9, 3.827381515209503*^9}, {3.827381549904497*^9, 
   3.827381553446374*^9}, {3.827381768459325*^9, 3.827381780195036*^9}, {
   3.827382119423181*^9, 3.8273821218284407`*^9}, {3.827382533673478*^9, 
   3.82738256975513*^9}, {3.827382700709693*^9, 3.827382775717449*^9}, {
   3.827383026234619*^9, 3.8273830429328413`*^9}, {3.827383349860138*^9, 
   3.827383352650197*^9}, 3.8274079677875233`*^9, {3.8274860091675787`*^9, 
   3.827486034022623*^9}, {3.827486067031404*^9, 3.827486075022333*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"26af8bc8-8ad9-4d8e-86d5-126b11fce491"],

Cell[TextData[StyleBox["Solve with linear programming", "Subsection"]], "Text",
 CellChangeTimes->{{3.827486236059462*^9, 
  3.8274862408116083`*^9}},ExpressionUUID->"2c7ea743-e5d2-4795-bdf9-\
b65661b05218"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "CFT", " ", "descrete", " ", "spectrum", " ", "with", " ", "cutoff", " ", 
    "and", " ", "unitarian", " ", "bounds"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"ScalarSpectrum", " ", "=", " ", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"\[CapitalDelta]", ",", " ", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{
        "\[CapitalDelta]", ",", "\[CapitalDelta]UB", ",", " ", 
         "\[CapitalDelta]max", ",", " ", "\[Delta]"}], "}"}]}], "]"}]}], 
    ";"}], "\n", 
   RowBox[{
    RowBox[{"SpinSpectrum", " ", "=", " ", 
     RowBox[{"Flatten", "[", 
      RowBox[{
       RowBox[{"Table", "[", 
        RowBox[{
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"\[CapitalDelta]", ",", " ", "l"}], "}"}], ",", " ", 
           RowBox[{"{", 
            RowBox[{"\[CapitalDelta]", ",", " ", 
             RowBox[{"d", " ", "-", " ", "2", " ", "+", " ", "l"}], ",", " ", 
             "\[CapitalDelta]max", ",", " ", "0.1"}], "}"}]}], "]"}], ",", 
         " ", 
         RowBox[{"{", 
          RowBox[{"l", ",", " ", "2", ",", " ", "lmax", ",", " ", "2"}], 
          "}"}]}], "]"}], ",", " ", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Spectrum", " ", "=", " ", 
     RowBox[{"Join", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"\[CapitalDelta]\[Phi]", ",", " ", "0"}], "}"}], "}"}], ",", 
       " ", "ScalarSpectrum", ",", " ", "SpinSpectrum"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"order", " ", "of", " ", "derivatives"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"DerivOrders", " ", "=", " ", 
     RowBox[{"Flatten", "[", 
      RowBox[{
       RowBox[{"Table", "[", 
        RowBox[{
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"n", ",", " ", 
             RowBox[{"s", "-", "n"}]}], "}"}], ",", " ", 
           RowBox[{"{", 
            RowBox[{"n", ",", " ", "0", ",", " ", "s"}], "}"}]}], "]"}], ",", 
         " ", 
         RowBox[{"{", 
          RowBox[{"s", ",", " ", "0", ",", " ", "\[CapitalLambda]"}], "}"}]}],
         "]"}], ",", " ", "1"}], "]"}]}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.827488496027049*^9, 3.827488517006584*^9}, {
   3.8274885631675043`*^9, 3.827488628773819*^9}, {3.8274887198134813`*^9, 
   3.8274888304855223`*^9}, 3.827489381391074*^9, {3.827489780570672*^9, 
   3.827489787403563*^9}, {3.827490127643641*^9, 3.8274901317191887`*^9}, {
   3.827490298240202*^9, 3.827490298864737*^9}, 3.8274911721646833`*^9, {
   3.827491755876524*^9, 3.827491771504594*^9}, {3.8275050242377453`*^9, 
   3.827505087655686*^9}, 3.82750533762915*^9, {3.827505435500414*^9, 
   3.8275054379400806`*^9}, {3.82750610500574*^9, 3.8275062028928328`*^9}},
 CellLabel->"In[12]:=",ExpressionUUID->"ea2b56f9-86ec-4606-9727-9fd8b2ec34ab"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "derivatives", " ", "of", " ", "conformal", " ", "blocks", " ", "at", " ", 
    "the", " ", "crossing", " ", "symmetric", " ", "point", " ", "for", " ", 
    "varoius", " ", "orders", " ", "and", " ", "spectral", " ", "values"}], 
   " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"DerivF2", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"\[CapitalDelta]_", ",", " ", "l_"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"n_", ",", " ", "m_"}], "}"}]}], "]"}], " ", ":=", " ", 
    RowBox[{
     RowBox[{"n", "!"}], " ", 
     RowBox[{"m", "!"}], 
     RowBox[{"SeriesCoefficient", "[", 
      RowBox[{
       RowBox[{"F2", "[", 
        RowBox[{
        "\[CapitalDelta]", ",", " ", "l", ",", " ", "\[CapitalDelta]\[Phi]", 
         ",", " ", "z", ",", " ", "zb"}], "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"z", ",", " ", 
         RowBox[{"1", "/", "2"}], ",", "n"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"zb", ",", " ", 
         RowBox[{"1", "/", "2"}], ",", " ", "m"}], "}"}]}], "]"}]}]}], " ", 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"SetSharedVariable", "[", "count", "]"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Monitor", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"SpinDerivF2", " ", "=", " ", 
        RowBox[{"ParallelTable", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"count", " ", "=", " ", "s"}], ";", 
           RowBox[{"Table", "[", 
            RowBox[{
             RowBox[{"DerivF2", "[", 
              RowBox[{"s", ",", " ", "ord"}], "]"}], ",", " ", 
             RowBox[{"{", 
              RowBox[{"ord", ",", "DerivOrders"}], "}"}]}], "]"}]}], ",", " ", 
          RowBox[{"{", 
           RowBox[{"s", ",", "SpinSpectrum"}], "}"}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{"ScalarDerivativeF2", " ", "=", " ", 
        RowBox[{"ParallelTable", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"count", " ", "=", " ", "s"}], ";", 
           RowBox[{"Table", "[", 
            RowBox[{
             RowBox[{"DerivF2", "[", 
              RowBox[{"s", ",", " ", "ord"}], "]"}], ",", " ", 
             RowBox[{"{", 
              RowBox[{"ord", ",", "DerivOrders"}], "}"}]}], "]"}]}], ",", " ", 
          RowBox[{"{", 
           RowBox[{"s", ",", " ", "ScalarSpectrum"}], "}"}]}], "]"}]}], ";"}],
       ",", "\[IndentingNewLine]", "count"}], "\[IndentingNewLine]", "]"}], 
    " ", "//", " ", "AbsoluteTiming"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[CapitalDelta]\[Phi]DerivF2", " ", "=", " ", 
     RowBox[{"ParallelTable", "[", 
      RowBox[{
       RowBox[{"DerivF2", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"\[CapitalDelta]\[Phi]", ",", " ", "0"}], "}"}], ",", " ", 
         "ord"}], "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"ord", ",", "DerivOrders"}], "}"}]}], "]"}]}], 
    ";"}]}]}]], "Input",
 CellChangeTimes->{{3.827488496027049*^9, 3.827488517006584*^9}, {
   3.8274885631675043`*^9, 3.827488628773819*^9}, {3.8274887198134813`*^9, 
   3.82748879915627*^9}, {3.827488884347048*^9, 3.827488935330948*^9}, {
   3.8274914672589083`*^9, 3.827491506482073*^9}, {3.827491704913319*^9, 
   3.827491718416526*^9}, 3.8275048901945047`*^9, {3.8275050975195713`*^9, 
   3.82750515957484*^9}, {3.827505203174328*^9, 3.8275052673418083`*^9}, {
   3.827505472156724*^9, 3.827505515483327*^9}, {3.827506173214954*^9, 
   3.8275061741140947`*^9}, {3.8275062063974257`*^9, 3.827506275924757*^9}, {
   3.8275073234984903`*^9, 3.827507328301125*^9}, {3.827509194571669*^9, 
   3.827509200748983*^9}, {3.827509627560669*^9, 3.827509635496045*^9}, {
   3.8275097530072203`*^9, 3.827509803669135*^9}, {3.8275101200277987`*^9, 
   3.827510123740034*^9}},
 CellLabel->"In[16]:=",ExpressionUUID->"4de179a0-9789-4b4c-914d-282ba7783451"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "solve", " ", "crossing", " ", "equations", " ", "with", " ", "linear", 
    " ", "programming", " ", "for", " ", "a", " ", "given", " ", "choice", 
    " ", "of", " ", "scalar", " ", "dimension", " ", "gap", " ", 
    "\[CapitalDelta]\[Epsilon]"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"IndexStart", "[", "\[CapitalDelta]\[Epsilon]_", "]"}], " ", ":=", 
    RowBox[{
     RowBox[{"Floor", "[", 
      FractionBox[
       RowBox[{"\[CapitalDelta]\[Epsilon]", "-", "\[CapitalDelta]UB"}], 
       "\[Delta]"], "]"}], " ", "+", " ", "1"}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"TryDerivF2", "[", "\[CapitalDelta]\[Epsilon]_", "]"}], " ", ":=",
     " ", 
    RowBox[{"Join", "[", 
     RowBox[{
      RowBox[{"{", "\[CapitalDelta]\[Phi]DerivF2", "}"}], ",", 
      RowBox[{"Part", "[", 
       RowBox[{"ScalarDerivativeF2", ",", " ", 
        RowBox[{
         RowBox[{"IndexStart", "[", "\[CapitalDelta]\[Epsilon]", "]"}], ";;", 
         " ", 
         RowBox[{"-", "1"}]}]}], "]"}], ",", " ", "SpinDerivF2"}], 
     "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.827488937846861*^9, 3.827488993730093*^9}, {
   3.8274890521940107`*^9, 3.827489067449436*^9}, {3.827489106505683*^9, 
   3.827489151392712*^9}, {3.82748934562362*^9, 3.827489457901952*^9}, {
   3.827489584780696*^9, 3.827489744299506*^9}, {3.827489775082741*^9, 
   3.827489796386422*^9}, {3.827489888533943*^9, 3.827489946663083*^9}, {
   3.827489996864523*^9, 3.827490034096631*^9}, {3.827492671570385*^9, 
   3.827492674063748*^9}, {3.827492730290524*^9, 3.827492739205574*^9}, 
   3.827492828298429*^9, {3.827492875502005*^9, 3.8274928993017273`*^9}, {
   3.8275052850105753`*^9, 3.827505340476985*^9}, {3.827505370588777*^9, 
   3.827505393756473*^9}, {3.827505465411743*^9, 3.827505466900586*^9}, {
   3.827505523771214*^9, 3.8275056093303423`*^9}, {3.827506392474494*^9, 
   3.827506445114452*^9}, {3.827507800418529*^9, 3.827507801149365*^9}, 
   3.8275088961574583`*^9},
 CellLabel->"In[20]:=",ExpressionUUID->"8da08ac0-9741-4bef-9efd-396992c1b34f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SolveCB", "[", "\[CapitalDelta]\[Epsilon]_", "]"}], " ", ":=", 
  RowBox[{"LinearProgramming", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{"0", ",", " ", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"IndexStart", "[", "\[CapitalDelta]max", "]"}], " ", "-", 
         " ", 
         RowBox[{"IndexStart", "[", "\[CapitalDelta]\[Epsilon]", "]"}], " ", 
         "+", "1"}], ")"}], "+", 
       RowBox[{"Length", "[", "SpinSpectrum", "]"}], "+", "1"}]}], "]"}], ",",
     "\[IndentingNewLine]", 
    RowBox[{"Transpose", "[", 
     RowBox[{"TryDerivF2", "[", "\[CapitalDelta]\[Epsilon]", "]"}], "]"}], 
    ",", "\[IndentingNewLine]", 
    RowBox[{"Join", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "1"}], ",", " ", "0"}], "}"}], "}"}], ",", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", " ", "0"}], "}"}], ",", " ", 
        RowBox[{
         RowBox[{"Length", "[", "DerivOrders", "]"}], "-", "1"}]}], "]"}]}], 
     "]"}]}], "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.8274899871846447`*^9, 3.827489993120619*^9}, {
   3.827490042553055*^9, 3.827490105967679*^9}, {3.8274902278384523`*^9, 
   3.827490228014348*^9}, {3.82749260110437*^9, 3.8274926169281673`*^9}, {
   3.827493160586351*^9, 3.827493183267158*^9}, {3.827493235854137*^9, 
   3.827493249922637*^9}, {3.827493322354271*^9, 3.827493329401861*^9}, {
   3.827493363277997*^9, 3.827493514764902*^9}, {3.827493556963015*^9, 
   3.827493589879795*^9}, {3.827493832125382*^9, 3.827493896284923*^9}, {
   3.827505650513879*^9, 3.8275057739537563`*^9}, 3.8275064786287117`*^9, {
   3.8275079167560167`*^9, 3.827507925196494*^9}, {3.827507977813848*^9, 
   3.827507981499729*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"5848a6c2-b128-4b27-992c-f1ad5e64b9ec"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "binary", " ", "search", " ", "for", " ", "the", " ", "optimal", " ", 
    "\[CapitalDelta]\[Epsilon]"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"SolveCBBool", "[", "\[CapitalDelta]\[Epsilon]_", "]"}], " ", ":=",
     " ", 
    RowBox[{
     RowBox[{"Length", "[", 
      RowBox[{"Check", "[", 
       RowBox[{
        RowBox[{"SolveCB", "[", "\[CapitalDelta]\[Epsilon]", "]"}], ",", " ", 
        
        RowBox[{"{", "}"}], ",", " ", 
        RowBox[{"LinearProgramming", "::", "lpsnf"}]}], "]"}], "]"}], " ", 
     ">", " ", "0"}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"BinarySearch", "[", 
     RowBox[{
     "f_", ",", " ", "true_", ",", " ", "false_", ",", " ", "thresh_"}], 
     "]"}], " ", ":=", " ", "\[IndentingNewLine]", 
    RowBox[{"Module", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"try", " ", "=", " ", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"true", " ", "+", " ", "false"}], ")"}], "/", "2"}]}], 
       "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"If", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"Abs", "[", 
          RowBox[{"true", " ", "-", " ", "false"}], "]"}], " ", "<", " ", 
         "thresh"}], ",", " ", "\[IndentingNewLine]", "false", ",", 
        "\[IndentingNewLine]", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"f", "[", "try", "]"}], ",", " ", 
          RowBox[{"BinarySearch", "[", 
           RowBox[{
           "f", ",", " ", "try", ",", " ", "false", ",", " ", "thresh"}], 
           "]"}], ",", " ", 
          RowBox[{"BinarySearch", "[", 
           RowBox[{
           "f", ",", " ", "true", ",", " ", "try", ",", " ", "thresh"}], 
           "]"}]}], "]"}]}], "\[IndentingNewLine]", "]"}]}], 
     "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
   RowBox[{"BinarySearch", "[", 
    RowBox[{
    "SolveCBBool", ",", " ", "\[CapitalDelta]UB", ",", " ", 
     "\[CapitalDelta]\[Epsilon]", ",", "\[Delta]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.827493671814258*^9, 3.827493716294011*^9}, {
  3.827493749293981*^9, 3.8274938004738092`*^9}, {3.827505777682569*^9, 
  3.827505823903977*^9}, {3.827506320651573*^9, 3.8275063781551228`*^9}, {
  3.827506449746664*^9, 3.827506496210369*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"4b5c6ba8-2c9e-4a9a-9ebb-1b76e7f5ca29"]
},
WindowSize->{1920, 1016},
WindowMargins->{{0, Automatic}, {0, Automatic}},
TaggingRules->{
 "WelcomeScreenSettings" -> {"FEStarting" -> False}, "TryRealOnly" -> False},
Magnification:>1.2 Inherited,
FrontEndVersion->"12.2 for Linux x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"7eecad6e-93d8-4a2c-a284-74e84d9d61cf"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 223, 4, 61, "Text",ExpressionUUID->"d6a2dfa9-7e02-45de-8e87-e4e7e6321cfd"],
Cell[784, 26, 193, 3, 49, "Text",ExpressionUUID->"96fd1152-ecda-493d-adbf-11ee484be6d7"],
Cell[980, 31, 2505, 62, 237, "Input",ExpressionUUID->"390af4cf-58a6-44f4-8645-64765d5e1a6f"],
Cell[3488, 95, 194, 3, 49, "Text",ExpressionUUID->"72bd05e2-926f-4fe8-94ff-2848935fc4f4"],
Cell[3685, 100, 3747, 100, 160, "Input",ExpressionUUID->"26af8bc8-8ad9-4d8e-86d5-126b11fce491"],
Cell[7435, 202, 207, 3, 49, "Text",ExpressionUUID->"2c7ea743-e5d2-4795-bdf9-b65661b05218"],
Cell[7645, 207, 3072, 75, 187, "Input",ExpressionUUID->"ea2b56f9-86ec-4606-9727-9fd8b2ec34ab"],
Cell[10720, 284, 3951, 90, 262, "Input",ExpressionUUID->"4de179a0-9789-4b4c-914d-282ba7783451"],
Cell[14674, 376, 2129, 42, 108, "Input",ExpressionUUID->"8da08ac0-9741-4bef-9efd-396992c1b34f"],
Cell[16806, 420, 1935, 42, 137, "Input",ExpressionUUID->"5848a6c2-b128-4b27-992c-f1ad5e64b9ec"],
Cell[18744, 464, 2452, 59, 312, "Input",ExpressionUUID->"4b5c6ba8-2c9e-4a9a-9ebb-1b76e7f5ca29"]
}
]
*)

